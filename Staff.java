package Superclass;

public class Staff extends Person {

	private double salary;

	public Staff(int id, String name, int age, double salary) {
		super(id, name, age);
		this.salary = salary;
	}

	public void showDetails() {
		System.out.println("ID: " + getId() + ", Name: " + getName() + ", Age: " + getAge() + ", Salary: " + salary);
	}

}
